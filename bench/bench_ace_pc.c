/*
 * RELIC is an Efficient LIbrary for Cryptography
 * Copyright (C) 2007-2015 RELIC Authors
 *
 * This file is part of RELIC. RELIC is legal property of its developers,
 * whose names are not listed here. Please refer to the COPYRIGHT file
 * for contact information.
 *
 * RELIC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * RELIC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RELIC. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * ACE: Testing pairing-based operations to understand the RELIC library.
 *
 * @version $Id$
 * @ingroup bench
 */
#include <stdio.h>
#include "relic.h"
#include "relic_bench.h"


int main(void) 
{
	g1_t x;

	if (core_init() != STS_OK) {
		core_clean();
		return 1;
	}

	if (pc_param_set_any() != STS_OK) {
		THROW(ERR_NO_CURVE);
		core_clean();
		return 0;
	}


	// util_banner("null", 0);
	// g1_null(x);

	// util_banner("new", 0);
	// g1_new(x);

	util_banner("rand", 0);
	g1_rand(x);

	g1_print(x);

	// util_banner("free", 0);
	g1_free(x);

	// util_banner("core_clean", 0);
	core_clean();
	return 0;
}
