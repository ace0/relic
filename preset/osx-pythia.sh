#!/bin/bash 
# These settings were selected to get very good performance on a 
# 2013 MacBook Air (OSX) using the BN-256 curve for the Pythia protocol.

cmake -DALLOC=AUTO -DARITH=gmp \
    -DFP_QNRES=off -DFP_PRIME=256 -DCHECK=off \
    -DCOMP="-O3 -funroll-loops -fomit-frame-pointer -march=native -mtune=native" \
    -DFP_METHD="BASIC;COMBA;COMBA;MONTY;LOWER;SLIDE" \
    -DFPX_METHD="INTEG;INTEG;LAZYR" \
    -DPP_METHD="LAZYR;OATEP" $1
