#!/bin/bash 
# These settings tune RELIC for 2013 MacBook Air (OSX) for BN-256 curve 
# performance for the Pythia protocol.

# -DARITH=easy \
# -DARITH=gmp \
# -DARITH=ASMXXX \

# -DCOMP="-Ofast" \
# -DCOMP="-O3 -funroll-loops -fomit-frame-pointer -march=native -mtune=native" \

# Settings form x64-asm config
#    -DFP_METHD="INTEG;INTEG;INTEG;MONTY;LOWER;SLIDE" \

cmake -DALLOC=AUTO -DARITH=gmp \
    -DFP_QNRES=on -DFP_PRIME=257 -DCHECK=off \
    -DCOMP="-O3 -funroll-loops -fomit-frame-pointer -march=native -mtune=native" \
    -DFP_METHD="BASIC;COMBA;COMBA;MONTY;LOWER;SLIDE" \
    -DFPX_METHD="INTEG;INTEG;LAZYR" \
    -DPP_METHD="LAZYR;OATEP" $1


# LEFT OFF:
# - Testing configurations
# - What about ARITH=easy, can we enable QNRES somehow?,
#   -Ofast, multithreading, are there other basic configs that should be tested?