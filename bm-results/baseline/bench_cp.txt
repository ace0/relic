-- RELIC 0.4.1 configuration:

** Allocation mode: AUTO

** Arithmetic backend: easy

** Benchmarking options:
   Number of times: 10000

** Multiple precision module options:
   Precision: 1024 bits, 16 words
   Arithmetic method: COMBA;COMBA;MONTY;SLIDE;BASIC;BASIC

** Prime field module options:
   Prime size: 256 bits, 4 words
   Arithmetic method: BASIC;COMBA;COMBA;MONTY;MONTY;SLIDE

** Prime field extension module options:
   Arithmetic method: BASIC;BASIC;BASIC

** Prime elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Bilinear pairing module options:
   Arithmetic method: BASIC;OATEP

** Binary field module options:
   Polynomial size: 283 bits, 5 words
   Arithmetic method: LODAH;TABLE;QUICK;QUICK;QUICK;QUICK;EXGCD;SLIDE;QUICK

** Binary elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Elliptic Curve Cryptography module options:
   Arithmetic method: PRIME

** Hash function module options:
   Chosen method: SH256


-- Benchmarks for the CP module:

-- Protocols based on integer factorization:

BENCH: cp_rsa_gen                       = 83162 microsec
BENCH: cp_rsa_enc                       = 345 microsec
BENCH: cp_rsa_dec                       = 1671 microsec
BENCH: cp_rsa_gen_basic                 = 79412 microsec
BENCH: cp_rsa_dec_basic                 = 2615 microsec
BENCH: cp_rsa_gen_quick                 = 112717 microsec
BENCH: cp_rsa_dec_quick                 = 1662 microsec
BENCH: cp_rsa_gen                       = 183046 microsec
BENCH: cp_rsa_sig (h = 0)               = 1683 microsec
BENCH: cp_rsa_sig (h = 1)               = 1691 microsec
BENCH: cp_rsa_ver (h = 0)               = 59 microsec
BENCH: cp_rsa_ver (h = 1)               = 58 microsec
BENCH: cp_rsa_gen_basic                 = 102341 microsec
BENCH: cp_rsa_sig_basic (h = 0)         = 2639 microsec
BENCH: cp_rsa_sig_basic (h = 1)         = 2698 microsec
BENCH: cp_rsa_gen_quick                 = 77448 microsec
BENCH: cp_rsa_sig_quick (h = 0)         = 1668 microsec
BENCH: cp_rsa_sig_quick (h = 1)         = 1671 microsec
BENCH: cp_rabin_gen                     = 330824 microsec
BENCH: cp_rabin_enc                     = 0 microsec
BENCH: cp_rabin_dec                     = 1700 microsec
BENCH: cp_bdpe_gen                      = 165608 microsec
BENCH: cp_bdpe_enc                      = 97 microsec
BENCH: cp_bdpe_dec                      = 7492 microsec
BENCH: cp_phpe_gen                      = 58598 microsec
BENCH: cp_phpe_enc                      = 2732 microsec
BENCH: cp_bdpe_dec                      = 1928 microsec

-- Protocols based on elliptic curves:

BENCH: cp_ecdh_gen                      = 908 microsec
BENCH: cp_ecdh_key                      = 2146 microsec
BENCH: cp_ecmqv_gen                     = 895 microsec
BENCH: cp_ecmqv_key                     = 2709 microsec
BENCH: cp_ecies_gen                     = 879 microsec
BENCH: cp_ecies_enc                     = 3038 microsec
BENCH: cp_ecies_dec                     = 2118 microsec
BENCH: cp_ecdsa_gen                     = 888 microsec
BENCH: cp_ecdsa_sign (h = 0)            = 1120 microsec
BENCH: cp_ecdsa_sign (h = 1)            = 1109 microsec
BENCH: cp_ecdsa_ver (h = 0)             = 2890 microsec
BENCH: cp_ecdsa_ver (h = 1)             = 2845 microsec
BENCH: cp_ecss_gen                      = 919 microsec
BENCH: cp_ecss_sign                     = 821 microsec
BENCH: cp_ecss_ver                      = 2282 microsec
BENCH: cp_vbnn_ibs_kgc_gen              = 750 microsec
BENCH: cp_vbnn_ibs_kgc_extract_key      = 781 microsec
BENCH: cp_vbnn_ibs_user_sign            = 772 microsec
BENCH: cp_vbnn_ibs_user_verify          = 4414 microsec

-- Protocols based on pairings:

BENCH: cp_sokaka_gen                    = 4 microsec
BENCH: cp_sokaka_gen_prv                = 8099 microsec
BENCH: cp_sokaka_key (g1)               = 8585 microsec
BENCH: cp_sokaka_key (g2)               = 10094 microsec
BENCH: cp_ibe_gen                       = 611 microsec
BENCH: cp_ibe_gen_prv                   = 5832 microsec
BENCH: cp_ibe_enc                       = 18994 microsec
BENCH: cp_ecies_dec                     = 0 microsec
BENCH: cp_bgn_gen                       = 6063 microsec
BENCH: cp_bgn_enc1                      = 1816 microsec
BENCH: cp_bgn_dec1 (10)                 = 2074 microsec
BENCH: cp_bgn_enc2                      = 5864 microsec
BENCH: cp_bgn_dec2 (10)                 = 6643 microsec
BENCH: cp_bgn_mul                       = 33149 microsec
BENCH: cp_bgn_dec (100)                 = 47378 microsec
BENCH: cp_bgn_add                       = 111 microsec
BENCH: cp_bls_gen                       = 1419 microsec
BENCH: cp_bls_sign                      = 1606 microsec
BENCH: cp_bls_ver                       = 16813 microsec
BENCH: cp_bbs_gen                       = 9609 microsec
BENCH: cp_bbs_sign (h = 0)              = 769 microsec
BENCH: cp_bbs_sign (h = 1)              = 768 microsec
BENCH: cp_bbs_ver (h = 0)               = 9547 microsec
BENCH: cp_bbs_ver (h = 1)               = 9505 microsec
