-- RELIC 0.4.1 configuration:

** Allocation mode: AUTO

** Arithmetic backend: easy

** Benchmarking options:
   Number of times: 10000

** Multiple precision module options:
   Precision: 1024 bits, 16 words
   Arithmetic method: COMBA;COMBA;MONTY;SLIDE;BASIC;BASIC

** Prime field module options:
   Prime size: 256 bits, 4 words
   Arithmetic method: BASIC;COMBA;COMBA;MONTY;MONTY;SLIDE

** Prime field extension module options:
   Arithmetic method: BASIC;BASIC;BASIC

** Prime elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Bilinear pairing module options:
   Arithmetic method: BASIC;OATEP

** Binary field module options:
   Polynomial size: 283 bits, 5 words
   Arithmetic method: LODAH;TABLE;QUICK;QUICK;QUICK;QUICK;EXGCD;SLIDE;QUICK

** Binary elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Elliptic Curve Cryptography module options:
   Arithmetic method: PRIME

** Hash function module options:
   Chosen method: SH256


-- Benchmarks for the PC module:

-- Curve BN-P256:

-- Group G_1:

** Utilities:

BENCH: g1_null                          = 0 microsec
BENCH: g1_new                           = 0 microsec
BENCH: g1_free                          = 0 microsec
BENCH: g1_is_infty                      = 0 microsec
BENCH: g1_set_infty                     = 0 microsec
BENCH: g1_copy                          = 0 microsec
BENCH: g1_cmp                           = 0 microsec
BENCH: g1_rand                          = 599 microsec
BENCH: g1_is_valid                      = 2 microsec
BENCH: g1_size_bin (0)                  = 0 microsec
BENCH: g1_size_bin (1)                  = 0 microsec
BENCH: g1_write_bin (0)                 = 2 microsec
BENCH: g1_write_bin (1)                 = 1 microsec
BENCH: g1_read_bin (0)                  = 6 microsec
BENCH: g1_read_bin (1)                  = 177 microsec

** Arithmetic:

BENCH: g1_add                           = 9 microsec
BENCH: g1_sub                           = 9 microsec
BENCH: g1_dbl                           = 4 microsec
BENCH: g1_neg                           = 0 microsec
BENCH: g1_mul                           = 1061 microsec
BENCH: g1_mul_gen                       = 582 microsec
BENCH: g1_mul_pre                       = 637 microsec
BENCH: g1_mul_fix                       = 587 microsec
BENCH: g1_mul_sim                       = 1566 microsec
BENCH: g1_mul_sim_gen                   = 1529 microsec
BENCH: g1_map                           = 364 microsec

-- Group G_2:

** Utilities:

BENCH: g2_null                          = 0 microsec
BENCH: g2_new                           = 0 microsec
BENCH: g2_free                          = 0 microsec
BENCH: g2_is_infty                      = 0 microsec
BENCH: g2_set_infty                     = 0 microsec
BENCH: g2_copy                          = 0 microsec
BENCH: g2_cmp                           = 0 microsec
BENCH: g2_rand                          = 4210 microsec
BENCH: g2_is_valid                      = 5 microsec
BENCH: g2_size_bin (0)                  = 0 microsec
BENCH: g2_size_bin (1)                  = 0 microsec
BENCH: g2_write_bin (0)                 = 5 microsec
BENCH: g2_write_bin (1)                 = 3 microsec
BENCH: g2_read_bin (0)                  = 12 microsec
BENCH: g2_read_bin (1)                  = 480 microsec

** Arithmetic:

BENCH: g2_add                           = 16 microsec
BENCH: g2_sub                           = 17 microsec
BENCH: g2_dbl                           = 9 microsec
BENCH: g2_neg                           = 0 microsec
BENCH: g2_mul                           = 4212 microsec
BENCH: g2_mul_gen                       = 1418 microsec
BENCH: g2_mul_pre                       = 2437 microsec
BENCH: g2_mul_fix                       = 1403 microsec
BENCH: g2_mul_sim                       = 4232 microsec
BENCH: g2_mul_sim_gen                   = 4213 microsec
BENCH: g2_map                           = 1543 microsec

-- Group G_T:

** Utilities:

BENCH: gt_null                          = 0 microsec
BENCH: gt_new                           = 0 microsec
BENCH: gt_free                          = 0 microsec
BENCH: gt_copy                          = 0 microsec
BENCH: gt_zero                          = 0 microsec
BENCH: gt_set_unity                     = 0 microsec
BENCH: gt_is_unity                      = 0 microsec
BENCH: gt_rand                          = 4217 microsec
BENCH: gt_cmp                           = 0 microsec
BENCH: gt_size_bin (0)                  = 0 microsec
BENCH: gt_size_bin (1)                  = 32 microsec
BENCH: gt_write_bin (0)                 = 15 microsec
BENCH: gt_write_bin (1)                 = 41 microsec
BENCH: gt_read_bin (0)                  = 35 microsec
BENCH: gt_read_bin (1)                  = 54 microsec

** Arithmetic:

BENCH: gt_mul                           = 24 microsec
BENCH: gt_sqr                           = 18 microsec
BENCH: gt_inv                           = 75 microsec
BENCH: gt_exp                           = 8305 microsec

-- Pairing:

** Arithmetic:

BENCH: pc_map                           = 8145 microsec
BENCH: pc_exp                           = 4173 microsec
