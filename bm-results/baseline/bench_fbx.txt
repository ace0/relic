-- RELIC 0.4.1 configuration:

** Allocation mode: AUTO

** Arithmetic backend: easy

** Benchmarking options:
   Number of times: 10000

** Multiple precision module options:
   Precision: 1024 bits, 16 words
   Arithmetic method: COMBA;COMBA;MONTY;SLIDE;BASIC;BASIC

** Prime field module options:
   Prime size: 256 bits, 4 words
   Arithmetic method: BASIC;COMBA;COMBA;MONTY;MONTY;SLIDE

** Prime field extension module options:
   Arithmetic method: BASIC;BASIC;BASIC

** Prime elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Bilinear pairing module options:
   Arithmetic method: BASIC;OATEP

** Binary field module options:
   Polynomial size: 283 bits, 5 words
   Arithmetic method: LODAH;TABLE;QUICK;QUICK;QUICK;QUICK;EXGCD;SLIDE;QUICK

** Binary elliptic curve module options:
   Arithmetic method: PROJC;LWNAF;COMBS;INTER

** Elliptic Curve Cryptography module options:
   Arithmetic method: PRIME

** Hash function module options:
   Chosen method: SH256


-- Benchmarks for the FBX module:

-- Irreducible pentanomial:
   z^283 + z^12 + z^7 + z^5 + 1

-- Quadratic extension:

** Utilities:

BENCH: fb2_null                         = 0 microsec
BENCH: fb2_new                          = 0 microsec
BENCH: fb2_free                         = 0 microsec
BENCH: fb2_copy                         = 0 microsec
BENCH: fb2_neg                          = 0 microsec
BENCH: fb2_zero                         = 0 microsec
BENCH: fb2_is_zero                      = 0 microsec
BENCH: fb2_rand                         = 6 microsec
BENCH: fb2_cmp                          = 0 microsec

** Arithmetic:

BENCH: fb2_add                          = 0 microsec
BENCH: fb2_sub                          = 0 microsec
BENCH: fb2_mul                          = 4 microsec
BENCH: fb2_sqr                          = 1 microsec
BENCH: fb2_slv                          = 2 microsec
BENCH: fb2_inv                          = 13 microsec
