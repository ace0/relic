#!/bin/bash 
# These settings tune RELIC for 2013 MacBook Air (OSX) for BN-256 curve 
# performance for the Pythia protocol.

#cmake -DALLOC=AUTO -DWORD=64 -DRAND=UDEV -DSHLIB=OFF -DSTBIN=ON -DTIMER=CYCLE
#-DCHECK=off -DVERBS=off  -DFP_PRIME=256
#-DFP_METHD="INTEG;INTEG;INTEG;MONTY;LOWER;SLIDE" -DCOMP="-O3 -funroll-loops
#-fomit-frame-pointer -finline-small-functions -march=native -mtune=native"
#-DFP_PMERS=off -DFP_QNRES=on -DFPX_METHD="INTEG;INTEG;LAZYR"
#-DPP_METHD="LAZYR;OATEP" $1

cmake -DALLOC=AUTO -DARITH=gmp $1